import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html'
})
export class LandingPage {

  slides = [
    {
      title: "Welcome to the World!",
      description: "Lorem ipsum",
      image: "assets/img/ica-slidebox-img-1.png",
    },
    {
       title: "Welcome to the World!",
      description: "Lorem ipsum",
      image: "assets/img/ica-slidebox-img-1.png",
    },
    {
      title: "Welcome to the World!",
      description: "Lorem ipsum",
      image: "assets/img/ica-slidebox-img-1.png",
    }
  ];


  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
  }
  goToSignup() {
    this.navCtrl.push(SignupPage);
  }

  goToLogin() {
    this.navCtrl.push(LoginPage);
  }

}
