import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { FirebaseService } from '../../providers/firebase-service';

declare var google;

@Component({
  selector: 'page-proceed-order',
  templateUrl: 'proceed-order.html'
})
export class ProceedOrderPage implements OnInit {
  @ViewChild('addressAutocomplete') autoCompleteElement: ElementRef;

  placeSearch; addressAutocomplete;
  place: any;
  event = {
    timeStarts: '12:00'
  }
  order: any; products: any; cost: any; shopContent: any;
  deliveryTimeType: any;
  currentDateTime: any;
  timmingType: string = 'anyTime';

  addressContent: Object = {
    name: '',
    street_number: '',
    route: '',
    locality: '',
    sublocality_level_1: '',
    administrative_area_level_1: '',
    country: '',
    postal_code: '',
    place_id: '',
    international_phone_number: '',
    formatted_address: '',
    formatted_phone_number: '',
  }
  orderForm: FormGroup;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public fs: FirebaseService) {

    this.order = navParams.get('order');
    this.products = navParams.get('products');
    this.cost = navParams.get('cost');
    this.shopContent = navParams.get('shopContent');

    this.currentDateTime = new Date();

    this.deliveryTimeType = new FormGroup({
      "deliveryTimeType": new FormControl({ value: 'anytime', disabled: false })
    });

    this.orderForm = formBuilder.group({
      address: ['', Validators.required],
      time: ['', Validators.required],
      reward: ['', Validators.required],
      anyTime: [{ value: 'choose time', disabled: false }],
      betweenTimeOne: [{ value: 'choose time', disabled: true }],
      betweenTimeTwo: [{ value: 'choose time', disabled: true }]
    });
  }

  ngOnInit() {
    console.log('ordering page initialized');
    // let input = document.getElementById('addressAutocomplete').getElementsByTagName('input')[0];
    // this.initaddressAutocomplete();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProceedOrderPage');
  }
  ngAfterViewInit() {
    let input = document.getElementById('addressAutocomplete').getElementsByTagName('input')[0];
    this.addressAutocomplete = new google.maps.places.Autocomplete(input);
    console.log(this.addressAutocomplete);
    // this.place = this.addressAutocomplete.getPlace();
  }

  radioSelect($event) {
    console.log('radio button selected')
    console.log($event);
    if ($event === 'specificTime') {
      this.orderForm.get('betweenTimeOne').enable();
      this.orderForm.get('betweenTimeTwo').enable();
      this.orderForm.get('anyTime').disable();
    } else if ($event === 'anyTime'){
      this.orderForm.get('betweenTimeOne').disable();
      this.orderForm.get('betweenTimeTwo').disable();
      this.orderForm.get('anyTime').enable();
    }
  }

  geolocate() {
    console.log('in focus');
    let geolocation = {
      lat: 31.5127918,
      lng: 74.3162381
    };
    let circle = new google.maps.Circle({
      center: geolocation,
      radius: 500
    });
    this.addressAutocomplete.setBounds(circle.getBounds());

    this.place = this.addressAutocomplete.getPlace();
    if (this.place) {
      this.storePlaceDetails(this.place);
    }
  }
  getPlaceDetails() {
    this.place = this.addressAutocomplete.getPlace();
    if (this.place) {
      this.storePlaceDetails(this.place);
    }
  }
  storePlaceDetails(place) {
    console.log(place);
    place.address_components.map(item => {
      console.log(item.types[0] + ': ' + item.long_name);
      this.addressContent[item.types[0]] = item.long_name;
    })
    this.addressContent['name'] = place.name;
    this.addressContent['place_id'] = place.place_id;
    this.addressContent['international_phone_number'] = place.international_phone_number;
    this.addressContent['formatted_address'] = place.formatted_address;
    this.addressContent['formatted_phone_number'] = place.formatted_phone_number;

    console.log(this.addressContent);
  }

  addOrder() {
    this.place = this.addressAutocomplete.getPlace();
    if (this.place) {
      this.storePlaceDetails(this.place);
    }
    this.fs.addOrder(
      this.shopContent,
      this.products,
      this.cost,
      this.addressContent,
      this.orderForm.value.address,
      this.orderForm.value.time,
      this.orderForm.value.reward)
  }
}
