import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
// importing provider
import { FirebaseService } from '../../providers/firebase-service';

@Component({
  selector: 'page-edit-modal',
  templateUrl: 'edit-modal.html'
})
export class EditModalPage {
  userOrder$: FirebaseListObservable<any>;
  orderUID: any;
  items = [];
  orderForm: FormGroup;
  initProd: any;
  shopName: any;
  cost: any;
  delivery_address: any;
  time: any;
  reward: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public fs: FirebaseService) {

    this.orderUID = navParams.get('orderUID');
    console.log(this.orderUID);
    this.getOrder(this.orderUID);

    this.orderForm = formBuilder.group({
      products: formBuilder.array([
        this.initProduct()
      ]),
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditModalPage');
  }

  initProduct() {
    this.initProd = this.formBuilder.group({
      product: ['', Validators.required]
    })
    return this.initProd;
  }

  dismiss() {
    this.fs.updateOrderStatus(this.orderUID, 'active')
      .then(() => {
        this.viewCtrl.dismiss();
      })
  }

  getOrder(orderUID) {

    this.fs.getSpecificOrder(orderUID).then(snapshot => {
      this.items.push(snapshot.val());
      // this.items = snapshot.val();
      console.log(this.items);
      this.shopName = this.items[0].shop.shop_name;
      this.cost = snapshot.val().cost;
      this.time = this.items[0].time;
      this.delivery_address = this.items[0].delivery_address.address;
      this.reward = this.items[0].reward;
      console.log(this.orderForm.value)
    })
  }

  addProduct(product?: FormArray) {

    const control = <FormArray>this.orderForm.controls['products'];
    control.push(this.initProduct());
  }

  removeProduct(i: number) {
    const control = <FormArray>this.orderForm.controls['products'];
    control.removeAt(i);
  }
}
