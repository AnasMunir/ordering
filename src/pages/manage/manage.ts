import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OfferPagePage } from '../offer-page/offer-page';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

@Component({
  selector: 'page-manage',
  templateUrl: 'manage.html'
})
export class ManagePage {
  orderList$: FirebaseListObservable<any>;
  items: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public af: AngularFire) {

    this.getActiveOrders();
  }

  offerNow(products, shopName, cost, reward) {
    this.navCtrl.push(OfferPagePage, {
      shopName: shopName,
      products: products,
      cost: cost,
      reward: reward
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManagePage');
  }

  getActiveOrders() {
    this.orderList$ = this.af.database.list('/orders');
    this.orderList$.subscribe(snapshot => {
      console.log(snapshot);
      this.items = snapshot
    })
    // console.log(this.orderList$);
  }
}
