import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { OrderingFormPage } from '../ordering-form/ordering-form';
import { TabsPage } from '../tabs/tabs';

import { AuthService } from '../../providers/auth-service';

import { EmailValidator } from '../../validators/email';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  submitAttempt: boolean = false;
  signupForm: FormGroup

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public auth: AuthService) {

    this.signupForm = formBuilder.group({
      userName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      address: [''],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signup() {
    this.submitAttempt = true;
    if (!this.signupForm.valid || this.signupForm.value.password !== this.signupForm.value.confirmPassword) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success!');
      console.log(this.signupForm.value);
      this.auth.signupUser(
        this.signupForm.value.email,
        this.signupForm.value.password,
        this.signupForm.value)
        .then(data => {
          console.log(data);
          this.navCtrl.setRoot(TabsPage);
        }, (error) => {
          console.log(error);
        });
    }
  }
}
