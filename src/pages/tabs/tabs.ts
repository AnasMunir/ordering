import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { OrderingFormPage } from '../ordering-form/ordering-form';
import { ManagePage } from '../manage/manage';
import { MyOrderPage } from '../my-order/my-order';
import { ProfilePage } from '../profile/profile';
import { DeliveryPagePage } from '../delivery-page/delivery-page';
/*
  Generated class for the Tabs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}
  tab1Root: any = ManagePage;
  tab2Root: any = MyOrderPage;
  tab3Root: any = OrderingFormPage;
  tab4Root: any = DeliveryPagePage;
  tab5Root: any = ProfilePage;
  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
