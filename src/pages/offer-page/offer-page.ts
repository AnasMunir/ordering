import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-offer-page',
  templateUrl: 'offer-page.html'
})
export class OfferPagePage {
  shopName: any; products = []; cost: any; reward: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.shopName = navParams.get('shopName');
    this.products = navParams.get('products');
    this.cost = navParams.get('cost');
    this.reward = navParams.get('reward');

    console.log(this.products);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferPagePage');
  }

}
