import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ProceedOrderPage } from '../proceed-order/proceed-order';

declare var google;

@Component({
  selector: 'page-ordering-form',
  templateUrl: 'ordering-form.html'
})
export class OrderingFormPage implements OnInit {
  @ViewChild('autocomplete') autoCompleteElement: ElementRef;

  placeSearch; autocomplete;
  place: any;
  shopContent: Object = {
    name: '',
    street_number: '',
    route: '',
    locality: '',
    sublocality_level_1: '',
    administrative_area_level_1: '',
    country: '',
    postal_code: '',
    place_id: '',
    international_phone_number: '',
    formatted_address: '',
    formatted_phone_number: '',
  }
  orderForm: FormGroup;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder) {

    this.orderForm = formBuilder.group({
      products: formBuilder.array([
        this.initProduct(),
      ]),
      ShopName: ['', Validators.required],
      cost: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    console.log('ordering page initialized');
    // this.initAutocomplete();
  }

  initProduct() {
    return this.formBuilder.group({
      product: ['', Validators.required]
    })
  }

  addProduct() {
    const control = <FormArray>this.orderForm.controls['products'];
    control.push(this.initProduct());
  }

  removeProduct(i: number) {
    const control = <FormArray>this.orderForm.controls['products'];
    control.removeAt(i);
  }

  ngAfterViewInit() {
    let input = document.getElementById('autocomplete').getElementsByTagName('input')[0];
    this.autocomplete = new google.maps.places.Autocomplete(input);
    console.log(this.autocomplete);
    // this.place = this.autocomplete.getPlace();
  }
  proceed() {
    console.log(this.orderForm.value);
    this.place = this.autocomplete.getPlace();
    if (this.place) {
      this.storePlaceDetails(this.place);
    }
    this.navCtrl.push(ProceedOrderPage, {
      order: this.orderForm.value,
      products: this.orderForm.value.products,
      cost: this.orderForm.value.cost,
      shopContent: this.shopContent
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderingFormPage');
  }

  geolocate() {
    console.log('in focus');
    let geolocation = {
      lat: 31.5127918,
      lng: 74.3162381
    };
    let circle = new google.maps.Circle({
      center: geolocation,
      radius: 500
    });
    this.autocomplete.setBounds(circle.getBounds());

    this.place = this.autocomplete.getPlace();
    if (this.place) {
      this.storePlaceDetails(this.place);
    }
  }
  getPlaceDetails() {
    this.place = this.autocomplete.getPlace();
    if (this.place) {
      this.storePlaceDetails(this.place);
    }
  }
  storePlaceDetails(place) {
    console.log(place);
    place.address_components.map(item => {
      console.log(item.types[0] + ': ' + item.long_name);
      this.shopContent[item.types[0]] = item.long_name;
    })
    this.shopContent['name'] = place.name;
    this.shopContent['place_id'] = place.place_id;
    if (place.international_phone_number) {
      this.shopContent['international_phone_number'] = place.international_phone_number;
    }
    this.shopContent['formatted_address'] = place.formatted_address;
    this.shopContent['formatted_phone_number'] = place.formatted_phone_number;

    console.log(this.shopContent);
  }
}
