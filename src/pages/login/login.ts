import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import { OrderingFormPage } from '../ordering-form/ordering-form';
import { TabsPage } from '../tabs/tabs';
import { SignupPage } from '../signup/signup';

import { AuthService } from '../../providers/auth-service';
import { EmailValidator } from '../../validators/email';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public submitAttempt: boolean = false;
  public loginForm: FormGroup;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public auth: AuthService) {

    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success!');
      console.log(this.loginForm.value);
      this.auth.loginUser(
        this.loginForm.value.email,
        this.loginForm.value.password,).then(data => {
          console.log(data);
          this.navCtrl.setRoot(TabsPage);
        }, (error) => {
          console.log(error);
        });
    }
  }
  goToSignup() {
    this.navCtrl.setRoot(SignupPage);
  }

}
