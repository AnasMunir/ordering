import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2';

import { EditModalPage } from '../edit-modal/edit-modal';
import { FirebaseService } from '../../providers/firebase-service';

@Component({
  selector: 'page-my-order',
  templateUrl: 'my-order.html'
})
export class MyOrderPage {

  userOrders$: FirebaseListObservable<any>;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public fs: FirebaseService) {

    this.getUserOrders();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyOrderPage');
  }

  getUserOrders() {
    this.userOrders$ = this.fs.getUserOrders();
  }
  deleteOrder(orderKey) {
    let status = this.fs.deleteOrder(orderKey);
    status.then(data => {
      console.log(data);
      console.log('order deleted');
    }, (error) => {
      console.error('Error deleting order: ' + error);
    })
  }

  editOrder(orderUID) {
    let editModal = this.modalCtrl.create(EditModalPage,
      {
        orderUID: orderUID
      });
    this.fs.updateOrderStatus(orderUID, 'in_active')
      .then(() => {
        editModal.present();
      }, error => {
        console.log('error updating status', error);
      });
  }
}
