import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// importing angularire 2 
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

// importign pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs/tabs';
import { ManagePage } from '../pages/manage/manage';
import { OrderingFormPage } from '../pages/ordering-form/ordering-form';
import { ProceedOrderPage } from '../pages/proceed-order/proceed-order';
import { DeliveryPagePage } from '../pages/delivery-page/delivery-page';
import { OfferPagePage } from '../pages/offer-page/offer-page';
import { ProfilePage } from '../pages/profile/profile';
import { LandingPage } from '../pages/landing/landing';
import { MyOrderPage } from '../pages/my-order/my-order';
import { EditModalPage } from '../pages/edit-modal/edit-modal';

// importing providers
import { AuthService } from '../providers/auth-service';
import { FirebaseService } from '../providers/firebase-service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

export const firebaseConfig = {
  apiKey: "AIzaSyAN2qPKANK-NMHjW8Rn1slqVlH71LohHiw",
  authDomain: "com.ionicframework.ordering401789",
  databaseURL: "https://tkride-user-1481096790043.firebaseio.com",
  storageBucket: "gs://tkride-user-1481096790043.appspot.com",
};

// Configuring Firebase auth
const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    TabsPage,
    OrderingFormPage,
    LandingPage,
    DeliveryPagePage,
    ProfilePage,
    ManagePage,
    ProceedOrderPage,
    MyOrderPage,
    OfferPagePage,
    EditModalPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    TabsPage,
    OrderingFormPage,
    LandingPage,
    DeliveryPagePage,
    ProfilePage,
    ManagePage,
    ProceedOrderPage,
    MyOrderPage,
    OfferPagePage,
    EditModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    FirebaseService
  ]
})
export class AppModule {}
