import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

// importing pages
import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ProfilePage } from '../pages/profile/profile';
import { ManagePage } from '../pages/manage/manage';
import { OrderingFormPage } from '../pages/ordering-form/ordering-form';
import { ProceedOrderPage } from '../pages/proceed-order/proceed-order';
import { LandingPage } from '../pages/landing/landing';
import { DeliveryPagePage } from '../pages/delivery-page/delivery-page';
import { MyOrderPage } from '../pages/my-order/my-order';
import { OfferPagePage } from '../pages/offer-page/offer-page';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, af: AngularFire) {
    const authObserver = af.auth.subscribe( user => {
      if (user) {
        this.rootPage = TabsPage;
        authObserver.unsubscribe();
      } else {
        this.rootPage = LandingPage;
        authObserver.unsubscribe();
      }
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
