import { Injectable } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import firebase from 'firebase';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  public fireAuth: any;
  public userData: any;

  constructor(public af: AngularFire) {
    console.log('Hello AuthService Provider');
    af.auth.subscribe(user => {
      if (user) {
        this.fireAuth = user.auth;
        console.log(user);
      }
    });
    this.fireAuth = firebase.auth();
    this.userData = firebase.database().ref('/users');
  }

  loginUser(email: string, password: string): firebase.Promise<any> {
    // return firebase.auth().signInWithEmailAndPassword(email, password);
    return this.af.auth.login({
      email: email,
      password: password
    });
  }

  signupUser(email: string, password: string, signupData: any): firebase.Promise<any> {
    return this.af.auth.createUser({ email: email, password: password })
      .then((newUser) => {
        this.userData.child(newUser.uid).set({
          email: email,
          userName: signupData.userName,
          address: signupData.address,
        });
      });

    /*return this.fireAuth.createUserWithEmailAndPassword(email, password)
      .then((newUser) => {
        this.userData.child(newUser.uid).set({
          email: email,
          userName: signupData.userName,
          address: signupData.address,
        });
      });*/
  }

  resetPassword(email: string): any {
    return this.fireAuth.sendPasswordResetEmail(email);
  }

  logoutUser(): any {
    return this.fireAuth.signOut();
  }

}
