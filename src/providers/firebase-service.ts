import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import firebase from 'firebase';

@Injectable()
export class FirebaseService {

  orderList$: FirebaseListObservable<any>;
  orderObject$: FirebaseObjectObservable<any>;
  buyerObject$: FirebaseObjectObservable<any>;
  buyerAddress: any; buyerEmail: any;
  userData: any; userOrders: any;
  curentUser = firebase.auth().currentUser;

  constructor(public http: Http, public af: AngularFire) {
    console.log('Hello FirebaseService Provider');
    this.orderList$ = af.database.list('/orders');
    this.orderObject$ = af.database.object('/orders');
    this.userData = firebase.database().ref('/users/' + this.curentUser.uid + '/orders/');

    this.buyerObject$ = af.database.object('/users/' + this.curentUser.uid + '/', { preserveSnapshot: true });
    this.buyerObject$.subscribe(snapshot => {
      this.buyerAddress = snapshot.val().address
    })
  }

  addOrder(shopContent, products, cost, addressContent, address, time, reward) {
    this.orderList$.push({
      status: 'active',
      cost: cost,
      reward: reward,
      delivery_time: time,
      buyer: {
        buyer_id: this.curentUser.uid,
        buyer_address: this.buyerAddress
      },
      shop: {
        shop_name: shopContent.name,
        address: shopContent.formatted_address,
        phone_number: shopContent.international_phone_number,
        place_id: shopContent.place_id,
        postal_code: shopContent.postal_code,
        locality: shopContent.locality,
        country: shopContent.country,
        sublocality: shopContent.sublocality_level_1,
      },
      delivery_address: {
        name: addressContent.name,
        address: addressContent.formatted_address,
        postal_code: addressContent.postal_code,
        place_id: addressContent.place_id,
        locality: addressContent.locality,
        street_number: addressContent.street_number,
      },
      products: products
    }).then(orderRef => {
      // let ref = firebase.database().ref('drivers/' + driver.uid);
      // console.log(orderRef.key);
      this.userData.child(orderRef.key).update({
          uid: orderRef.key,
          status: 'active',
          cost: cost,
          reward: reward,
          delivery_time: time,
          shop: {
            shop_name: shopContent.name,
            address: shopContent.formatted_address,
            phone_number: shopContent.international_phone_number,
            place_id: shopContent.place_id,
            postal_code: shopContent.postal_code,
            locality: shopContent.locality,
            country: shopContent.country,
            sublocality: shopContent.sublocality_level_1,
          },
          delivery_address: {
            name: addressContent.name,
            address: addressContent.formatted_address,
            postal_code: addressContent.postal_code,
            place_id: addressContent.place_id,
            locality: addressContent.locality,
            street_number: addressContent.street_number,
          },
          products: products
        })

    }, (error) => {
      console.error('some error happend: ' + error);
    })
    
  }

  editOrderStatus(orderKey) {
    let orderRef = firebase.database().ref('/orders/'+orderKey);
    return orderRef.update({
      status: 'inactive'
    }, (error) => {
      console.log('Error updating status: ' + error);
    })
  }
  deleteOrder(orderKey) {
    let ref  = firebase.database().ref('/users/' + this.curentUser.uid + '/orders/' + orderKey);
    let orderRef = firebase.database().ref('/orders/'+orderKey);
    return ref.remove().then(data => {
      console.log(orderKey + ' removed');
      console.log(data);
      orderRef.remove().then(data => {
        console.log('order with '+ orderKey + ' removed');
      }, (error) => {
        console.error('Error removing from orders: ' + error);
      })
    }, (error) => {
      console.error('Error removing from users ' + error);
    });
  }

  getUserOrders() {
    // let userOrders$: FirebaseListObservable<any>;
    // userOrders$ = this.af.database.list('/users/' + this.curentUser.uid + '/orders/');
    return this.af.database.list('/users/' + this.curentUser.uid + '/orders/');
  }
  getSpecificOrder(orderUID) {
    // return this.af.database.list('/users/' + this.curentUser.uid + '/orders/'+ orderUID);
    let ref = firebase.database().ref('/users/' + this.curentUser.uid + '/orders/' + orderUID);
    return ref.once("value");
  }

  updateOrderStatus(orderUID, status: string) {
    const orderObject$ = this.af.database.object('orders/' + orderUID);
    return orderObject$.update({ status: status });
  }
}
